# déier

Projet ESGI

Clone le projet:

```$ git clone https://gitlab.com/alexxandre80/deier.git```

```$ cd deier```

Recuperer les submodules:

```$ git submodule init```

```$ git submodule update```

Mettre à jour un dossier "sous-module":

```$ cd <folder>```

Verifier si on est sur la branch du sous-module:

```$ git status```

```(HEAD detached from fc2b219)```

Recuperer la branch:

```$ git pull <remote> <branch>```
